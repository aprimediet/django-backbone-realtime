from __future__ import unicode_literals
from django.db import models


class Notification(models.Model):

    content_type = models.CharField(max_length=16)
    content_id = models.IntegerField()
    author = models.ForeignKey(
        'auth.User',
        related_name='notification_author')
    destination = models.ForeignKey(
        'auth.User',
        related_name='notification_to')
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
