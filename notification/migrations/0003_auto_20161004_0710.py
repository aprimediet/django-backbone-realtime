# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-04 07:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0002_auto_20161004_0619'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notification',
            old_name='on',
            new_name='created',
        ),
    ]
