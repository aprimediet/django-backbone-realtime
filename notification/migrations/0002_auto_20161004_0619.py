# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-04 06:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notification',
            old_name='to',
            new_name='destination',
        ),
        migrations.AddField(
            model_name='notification',
            name='content_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
