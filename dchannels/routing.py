from channels import include
from channels.routing import route
from messaging.consumers import (
    ws_connect_new, ws_echo_new,
    ws_disconnect_new,
    ws_connect_read, ws_echo_read,
    ws_disconnect_read)

message_new = [
    route('websocket.connect', ws_connect_new),
    route('websocket.receive', ws_echo_new),
    route('websocket.disconnect', ws_disconnect_new),
]

message_read = [
    route('websocket.connect', ws_connect_read),
    route('websocket.receive', ws_echo_read),
    route('websocket.disconnect', ws_disconnect_read),
]

channel_routing = [
    include(message_new, path=r"^/message-new/(?P<user>\w+)$"),
    include(message_read, path=r"^/message-read/(?P<user>\w+)$")
]
