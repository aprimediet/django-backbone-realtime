function waitForConnection(ws, cb, interval) {
  if(ws.readyState === 1) {
    cb();
  }
  else {
    setTimeout(function() {
      waitForConnection(cb, interval);
    }, interval);
  }
}

$(document).ready(function() {
  var ws,
      username = prompt('Please enter your username');

  if(username !== null && username !== undefined && username !== '') {
    $('#username').text(username);
    ws = new WebSocket('ws://192.168.0.39:8000/chat/default?username='+username);

    ws.onopen = function() {
      waitForConnection(ws, function() {
        console.log('Channel '+username+' opened');
      }, 1000);
    };

    ws.onerror = function(error) {
      alert('Websocket Error: ', error);
    };

    ws.onmessage = function(e) {
      var data = JSON.parse(e.data),
          uThem = data.username,
          msg = data.message,
          chatThem = '<div class="media">' +
                     '<div class="media-body lawan">' +
                     '<h4 class="media-heading">'+uThem+'</h4>' +
                     msg +
                     '<div></div>';
      if(uThem !== username){
        $('#messages').append(chatThem);
      }
    };

    var chatSend = function(message) {
      var chatMe = '<div class="media">' +
                   '<div class="media-body saya text-right">' +
                   message +
                   '<div></div>';
        ws.send(message);
      $('#messages').append(chatMe);
    };

    $('form').on('submit', function(ev) {
      var msg = $('#messageText').val();
      chatSend(msg);
      this.reset();
    });
  }
  else {
    alert('You have to input username!, please reload this page');
  }
});
