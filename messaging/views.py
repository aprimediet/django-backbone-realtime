from django.shortcuts import render
from rest_framework import viewsets, mixins
from .models import Message
from .serializers import MessageSerializer


class MessageView(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Message.objects.none()
    serializer_class = MessageSerializer

    def list(self, request):
        user = request.user
        self.queryset = Message.objects.filter(destination=user)

        return super(MessageView, self).list(request)

    def retrieve(self, request, pk=None):
        self.queryset = Message.objects.filter(id=pk)

        return super(MessageView, self).retrieve(request, pk)


class SentMessageView(mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    queryset = Message.objects.none()
    serializer_class = MessageSerializer

    def list(self, request):
        user = request.user
        self.queryset = Message.objects.filter(author=user)

        return super(SentMessageView, self).list(request)
