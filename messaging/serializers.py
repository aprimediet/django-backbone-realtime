from rest_framework import serializers
from .models import Message


class MessageSerializer(serializers.ModelSerializer):
    # author = serializers.ReadOnlyField()

    class Meta:
        model = Message
        fields = (
            'id',
            'destination',
            'author',
            'created',
            'content',)
        read_only_fields = (
            'author',)

    def create(self, validated_data):
        # Get user
        request = self.context.get('request')
        user = request.user

        obj = Message.objects.create(
            author=user,
            **validated_data)

        return obj
