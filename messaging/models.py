from __future__ import unicode_literals
from django.db import models


class Message(models.Model):
    destination = models.ForeignKey(
        'auth.User',
        related_name='message_destination')
    author = models.ForeignKey(
        'auth.User',
        related_name='message_author')
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return self.content
