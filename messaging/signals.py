from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from .models import Message
from notification.models import Notification
from .consumers import ws_echo_new


@receiver(post_save, sender=Message)
def model_post_save(sender, **kwargs):
    instance = kwargs['instance']
    content = instance.content

    content_text = (content[:120] + '...') if len(content) > 120 else content

    message = "%s is sent you a message: %s" % (
        instance.author.username, content_text)

    new_notification = Notification.objects.create(
        content_type='message',
        content_id=instance.id,
        author=instance.author,
        destination=instance.destination,
        content=message,
        created=instance.created
    )

    ws_echo_new({
        'id': new_notification.id,
        'content_type': new_notification.content_type,
        'content_id': new_notification.content_id,
        'author': new_notification.author.username,
        'author_id': new_notification.author.id,
        'destination': new_notification.destination.username,
        'destination_id': new_notification.destination.id,
        'content': new_notification.content,
        'created': new_notification.created.strftime('%A, %d %B %Y'),
        'is_read': new_notification.is_read
    })
