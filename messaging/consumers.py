import json
import urlparse
import logging
from channels import Group
from django.contrib.auth.models import User
from notification.models import Notification

logger = logging.getLogger(__name__)


def ws_echo_new(message):
    """
        Sending notification
        when user received a new message
    """
    channel_name = "message-new-%s" % message['destination']
    Group(channel_name).send({
        'text': json.dumps(message)
    })


def ws_connect_new(message, user):
    """
        Sending notification
        when user received a new message
    """
    channel_name = "message-new-%s" % user
    Group(channel_name).add(message.reply_channel)
    # Get notification when user is connected to channel
    notification = Notification.objects.filter(
        destination__username=user,
        is_read=False)

    if len(notification) > 0:
        for n in notification:
            ws_echo_new({
                'id': n.id,
                'content_type': n.content_type,
                'content_id': n.content_id,
                'author': n.author.username,
                'author_id': n.author.id,
                'destination': n.destination.username,
                'destination_id': n.destination.id,
                'content': n.content,
                'created': n.created.strftime('%A, %d %B %Y'),
                'is_read': n.is_read
            })


def ws_disconnect_new(message):
    """
        Sending notification
        when user received a new message
    """
    user = message.channel_session['user']
    channel_name = 'message-new-%s' % user

    print "Disconnecting from channel %s" % channel_name
    Group(channel_name).discard(message.reply_channel)


def ws_connect_read(message, user):
    """
        Sending data when user is reading a
        notification
    """
    channel_name = "message-read-%s" % user
    Group(channel_name).add(message.reply_channel)


def ws_echo_read(message, user):
    """
        Sending data when user is reading a
        notification
    """
    data = message.content['text']
    data = json.loads(data)
    notification = Notification.objects.get(id=data['id'])
    notification.is_read = True
    notification.save()

    channel_name = "message-read-%s" % user
    Group(channel_name).send({
        'text': json.dumps(data)
    })


def ws_disconnect_read(message, user):
    """
        Sending data when user is reading a
        notification
    """
    channel_name = 'message-read-%s' % user

    Group(channel_name).discard(message.reply_channel)
